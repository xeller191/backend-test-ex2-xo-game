import { NextResponse, NextRequest } from "next/server";

type ResponseData = {
  xoSelectIdex: string;
};

type Body = {
  arrayXoBoard: (string | null)[];
};

const WIN_CONDITIONS: number[][] = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

const isBotIndexMove = (currentBoard: (string | null)[]): number => {
  for (const [x, y, z] of WIN_CONDITIONS) {
    const arrayXO = [currentBoard[x], currentBoard[y], currentBoard[z]];
    const nullCount = arrayXO.filter((value) => value === null).length;

    if (nullCount === 1 && new Set(arrayXO.filter(i => i !== null)).size === 1) {
        console.log("Case 1: Bot can win");
        
        const nullIndex = arrayXO.indexOf(null);
        arrayXO[nullIndex] = "O";
        
        console.log("Updated arrayXO:", arrayXO);
        
        if (arrayXO[0] === "O" && arrayXO[1] === "O" && arrayXO[2] === "O") {
            console.log("Bot wins!");
            return [x, y, z][nullIndex];
        }
    }
}


  let botMoveIndex = -1
  for (const element of WIN_CONDITIONS) {
    const [x, y, z] = element;
    const arrayXO = [currentBoard[x], currentBoard[y], currentBoard[z]];
    const arrayXORemoveNull = arrayXO.filter(i => i !== null)

    const have1NullValue =
      arrayXO.filter((value) => value === null).length === 1; //มีค่า null 1 ค่า
    const duplicate2Value = Array.from(new Set(arrayXORemoveNull)).length === 1; // ซ้ำกัน 1 ค่า
    const nullIndex = arrayXO.indexOf(null);

    const winCondition =  have1NullValue && duplicate2Value && nullIndex !== -1
    if (winCondition) { //nullIndex !== -1 ต้องเจอค่า null
      if(nullIndex===0){
        botMoveIndex = x
      }else if(nullIndex===1){
        botMoveIndex = y
      }else{
        botMoveIndex = z
      }
      break;
    }

    if(currentBoard[x] === null){
      botMoveIndex = x
    }else if(currentBoard[y] === null){
      botMoveIndex = y
    }else{
      botMoveIndex = z
    }

  }
  console.log({botMoveIndex})

  return botMoveIndex;
};


export async function POST(request: NextRequest) {
  const data: Body = await request.json();
  console.log(data.arrayXoBoard[0]);
  // ...
  return NextResponse.json({ xoSelectIdex: isBotIndexMove(data.arrayXoBoard) });
}
