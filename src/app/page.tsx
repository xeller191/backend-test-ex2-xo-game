'use client'
import React, { useCallback, useEffect, useState } from "react";

import { Board } from "./components/bord";
import { boxMove } from "./services/bot.service";

interface Scores {
  xScore: number;
  oScore: number;
}

export default function Page() {
  const [xPlaying, setXPlaying] = useState<boolean>(true);
  const [board, setBoard] = useState<(string | null)[]>(Array(9).fill(null));
  const [gameOver, setGameOver] = useState<boolean>(false);


  const getBoxMoveApi = async (boardData:(string | null)[]) =>{
    const body = {arrayXoBoard:boardData}
    const indexBotMove = await boxMove(body)
    return indexBotMove.xoSelectIdex
  }

  const WIN_CONDITIONS: number[][] = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  const handleBoxClick = (boxIdx: number): void => {
    const updatedBoard = board.map((value, idx) => {
      if (idx === boxIdx) {
        return xPlaying ? "X" : "O";
      } else {
        return value;
      }
    });
    

    if(board.filter(x=>x === null).length !==0){
      setBoard(updatedBoard);

      checkWinner(updatedBoard);
  
      setXPlaying(!xPlaying);
    }else{
      setGameOver(true)
    }
   
  };

  // const calculateNextMove = (currentBoard: (string | null)[]): number => {
  //   console.log({currentBoard})
  //   for (let i = 0; i < currentBoard.length; i++) {
  //     if (!currentBoard[i]) {
  //       return i;
  //     }
  //   }
  //   return -1;
  // };

  const calBotMov = useCallback( async ()=>{
    const indexBotMov = await getBoxMoveApi(board)
      handleBoxClick(indexBotMov)
  },[board,gameOver])

  useEffect(()=>{
    if(!xPlaying){
      calBotMov()
    }
  },[xPlaying,calBotMov])

  const checkWinner = (currentBoard: (string | null)[]): string | undefined => {
    for (const element of WIN_CONDITIONS) {
      const [x, y, z] = element;
      if (currentBoard[x] && currentBoard[x] === currentBoard[y] && currentBoard[y] === currentBoard[z]) {
        setGameOver(true);
        return currentBoard[x] as string;
      }
    }
  };

  useEffect(()=>{
    setTimeout(()=>{
      setGameOver(false);
      setBoard(Array(9).fill(null));
    },1000)
  },[gameOver])

  return (
    <div className="flex flex-col items-center justify-center ">
      <Board board={board} onClick={ handleBoxClick} />
    </div>
  );
}

/**
 *
0 | 1 | 2
----------
3 | 4 | 
----------
6 | 7 | X
 */