import { Box } from "./box";
type Props= {
  board:(string | null)[],
  onClick: any,//(boxIdx: number, playing: string) => void
}
export const Board = ({ board, onClick  }:Props) => {
    return (
      <div className="grid grid-cols-3 place-items-center justify-center w-80">
        {
          board.map((value:string | null, idx:number) => {
            return <Box key={idx} value={value} onClick={() => value === null && onClick(idx)} />;
          })
        }
      </div>
    )
  }