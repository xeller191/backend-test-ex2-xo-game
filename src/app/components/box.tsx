interface BoxProps {
    value: string | null;
    onClick:  any
}
export const Box = ({ value, onClick }:BoxProps) => {
    const box = "bg-white  shadow-md w-20 h-20 text-center text-5xl font-bold  leading-20 m-2"
    const boxX = "text-red-900"
    const box0 = "text-blue-900"
    const style = value === "X" ? `${box} ${boxX}` : `${box} ${box0}`;

    return (
        <button className={style} onClick={()=>onClick(parseInt(value!))}>{value}</button>
    )
}